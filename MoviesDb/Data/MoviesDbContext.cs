using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using MoviesDb.Models;

namespace MoviesDb.Data;

public class MoviesDbContext(DbContextOptions<MoviesDbContext> options) : IdentityDbContext(options)
{
    public DbSet<Models.Movie> Movie { get; set; }

public DbSet<MoviesDb.Models.Director> Director { get; set; } = default!;
}