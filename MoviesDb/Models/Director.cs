using System.ComponentModel.DataAnnotations;

namespace MoviesDb.Models;

public class Director
{
    public int Id { get; set; }
    [Required, StringLength(60)] public string? FirstName { get; set; }
    [Required, StringLength(60)] public string? LastName { get; set; }
    [DataType(DataType.Date), Required] public DateTime Birthday { get; set; }
    [Display(Name = "Number of Movies")]
    public int NumMovies { get; set; }
}