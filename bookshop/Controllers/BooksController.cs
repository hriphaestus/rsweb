﻿using bookshop.Data;
using bookshop.Interfaces;
using bookshop.Models;
using bookshop.ViewModels;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Rendering;
using Microsoft.EntityFrameworkCore;

namespace bookshop.Controllers;

public class BooksController(
    BookshopContext context,
    IBufferedFileUploadService bufferedFileUploadService)
    : Controller
{
    // GET: Books
    public async Task<IActionResult> Index(string searchString)
    {
        var books = context.Book.AsQueryable();

        if (!string.IsNullOrEmpty(searchString)) books = books.Where(x => x.Title.Contains(searchString));

        books = books.Include(x => x.Author)
            .Include(x => x.Reviews)
            .Include(x => x.Genres!).ThenInclude(x => x.Genre);

        var bookTitleVm = new BookTitleViewModel
        {
            Books = await books.ToListAsync(),
            Genres = await context.Genre.ToListAsync()
        };

        return View(bookTitleVm);
    }

    // GET: Books/Details/5
    public async Task<IActionResult> Details(int? id)
    {
        if (id == null || context.Book == null) return NotFound();

        var book = await context.Book
            .Include(b => b.Author)
            .Include(b => b.Genres)
            .FirstOrDefaultAsync(m => m.Id == id);

        if (book == null) return NotFound();

        return View(book);
    }

    // GET: Books/Create
    [Authorize(Roles = "Admin")]
    public IActionResult Create()
    {
        var genres = context.Genre.ToList();
        var viewModel = new BookCreateViewModel
        {
            Book = new Book(),
            GenreList = genres.Select(g => new SelectListItem
            {
                Value = g.Id.ToString(),
                Text = g.GenreName
            })
        };

        ViewData["AuthorId"] = new SelectList(context.Author, "Id", "FullName");
        return View(viewModel);
    }

    // POST: Books/Create
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> Create(BookCreateViewModel viewModel)
    {
        if (ModelState.IsValid)
        {
            context.Add(viewModel.Book);
            await context.SaveChangesAsync();

            if (viewModel.file != null)
            {
                var imagePath = await bufferedFileUploadService.UploadFile(viewModel.file, "images");

                if (imagePath != "none")
                    ViewBag.Message = "File Upload Successful!";
                else
                    ViewBag.Message = "File Upload Failed!";
                viewModel.Book.FrontPage = "/images/" + imagePath;
            }

            if (viewModel.fileBook != null)
            {
                var bookPath = await bufferedFileUploadService.UploadFile(viewModel.fileBook, "books");

                if (bookPath != "none")
                    ViewBag.Message = "File Upload Successful!";
                else
                    ViewBag.Message = "File Upload Failed!";
                viewModel.Book.DownloadUrl = "/books/" + bookPath;
            }

            if (viewModel.SelectedGenres != null)
                foreach (var genreId in viewModel.SelectedGenres)
                {
                    var bookGenre = new BookGenre { BookId = viewModel.Book.Id, GenreId = genreId };
                    context.Add(bookGenre);
                }

            await context.SaveChangesAsync();
            return RedirectToAction(nameof(Index));
        }

        ViewData["AuthorId"] = new SelectList(context.Author, "Id", "FullName", viewModel.Book.AuthorId);
        viewModel.GenreList = context.Genre.Select(g => new SelectListItem
        {
            Value = g.Id.ToString(),
            Text = g.GenreName
        });

        return View(viewModel.Book);
    }

    // GET: Books/Edit/5
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> Edit(int? id)
    {
        if (id == null || context.Book == null) return NotFound();

        var book = context.Book.Where(x => x.Id == id).Include(x => x.Genres).First();

        //var book = await _context.Book.FindAsync(id);
        if (book == null) return NotFound();

        var genres = context.Genre.AsEnumerable();

        var viewmodel = new BooksEditViewModel
        {
            Book = book,
            GenreList = new MultiSelectList(genres, "Id", "GenreName"),
            SelectedGenres = book.Genres!.Select(x => x.GenreId)
        };

        ViewData["AuthorId"] = new SelectList(context.Author, "Id", "FullName", book.AuthorId);
        return View(viewmodel);
    }

    // POST: Books/Edit/5
    // To protect from overposting attacks, enable the specific properties you want to bind to.
    // For more details, see http://go.microsoft.com/fwlink/?LinkId=317598.
    [HttpPost]
    [ValidateAntiForgeryToken]
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> Edit(int id, BooksEditViewModel viewModel)
    {
        if (id != viewModel.Book?.Id) return NotFound();

        if (ModelState.IsValid)
        {
            try
            {
                context.Update(viewModel.Book);
                await context.SaveChangesAsync();

                var newGenreList = viewModel.SelectedGenres!;
                IEnumerable<int> prevGenreList = context.BookGenre.Where(s => s.BookId == id).Select(s => s.GenreId);
                var toBeRemoved = context.BookGenre.Where(s => s.BookId == id);

                if (newGenreList != null)
                {
                    toBeRemoved = toBeRemoved.Where(s => !newGenreList.Contains(s.GenreId));
                    foreach (var genreId in newGenreList)
                        if (!prevGenreList.Any(s => s == genreId))
                            context.BookGenre.Add(new BookGenre { GenreId = genreId, BookId = id });
                }

                context.BookGenre.RemoveRange(toBeRemoved);
                await context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!BookExists(viewModel.Book.Id))
                    return NotFound();
                throw;
            }

            return RedirectToAction(nameof(Index));
        }

        ViewData["AuthorId"] = new SelectList(context.Author, "Id", "FullName", viewModel.Book.AuthorId);
        return View(viewModel);
    }

    // GET: Books/Delete/5
    [Authorize(Roles = "Admin")]
    public async Task<IActionResult> Delete(int? id)
    {
        if (id == null || context.Book == null) return NotFound();

        var book = await context.Book
            .Include(b => b.Author)
            .FirstOrDefaultAsync(m => m.Id == id);
        if (book == null) return NotFound();

        return View(book);
    }

    // POST: Books/Delete/5
    [HttpPost]
    [ActionName("Delete")]
    [ValidateAntiForgeryToken]
    public async Task<IActionResult> DeleteConfirmed(int id)
    {
        if (context.Book == null) return Problem("Entity set 'bookshopContext.Book'  is null.");

        var bookGenres = await context.BookGenre.Where(bg => bg.BookId == id).ToListAsync();
        context.BookGenre.RemoveRange(bookGenres);


        var book = await context.Book.FindAsync(id);
        if (book != null) context.Book.Remove(book);

        await context.SaveChangesAsync();
        return RedirectToAction(nameof(Index));
    }

    private bool BookExists(int id)
    {
        return (context.Book?.Any(e => e.Id == id)).GetValueOrDefault();
    }

    public async Task<IActionResult> GetPdf(string url)
    {
        var path = Path.Combine(
            Directory.GetCurrentDirectory(), "wwwroot/" + url);

        var memory = new MemoryStream();
        using (var stream = new FileStream(path, FileMode.Open))
        {
            await stream.CopyToAsync(memory);
        }

        memory.Position = 0;
        return File(memory, "application/pdf", "Demo.pdf");
    }
}